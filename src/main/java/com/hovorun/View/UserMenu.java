package com.hovorun.View;

import com.hovorun.Controller.ControllerImpl;
import com.hovorun.Controller.IController;

import java.util.Scanner;

public class UserMenu {
    IController controller = new ControllerImpl();
    Scanner scanner = new Scanner(System.in);

    public void show() {

        while (true){
            System.out.println("Get list of all goods in the mall: 1");
            System.out.println("Get list of goods cheaper than: 2");
            System.out.println("Get list of goods by category: 3");

            String string = scanner.nextLine();

            switch (string){
                case "1":
                    System.out.println(controller.generateGoodsList());
                    break;
                case "2":
                    System.out.println("Enter desired price: ");
                    double priceWanted = Double.parseDouble(scanner.nextLine());
                    System.out.println(controller.generateGoodsLessOrEqualByPrice(priceWanted));
                    break;
                case "3":
                    System.out.println("Categories: Wooden goods, Plumbing goods");
                    System.out.println("Enter desired category: ");
                    String category = scanner.nextLine();
                    System.out.println(controller.getGoodsByCategory(category));
                    break;
                default: return;
            }
        }

    }


}
