package com.hovorun.Model;

import com.hovorun.CustomException;

import java.util.List;

public interface IModel {

    List <Goods> findGoodsCheaperOrEqual(double priceWanted) throws CustomException;

    List <Goods> getAllGoodsList();

    List<Goods> getGoodsByCategory(String category);
}
